docker run --name="boat_det" -d --rm --gpus all --ipc=host \
    -v $PWD:/boats_det_v1 \
    -v /mnt/smb:/mnt/smb \
    -v /home1:/home1 \
    -w /boats_det_v1 \
    localhost:5000/pytorch:1.1.0-py3.7-cuda9.0_devel_psenet_flask sh -c \
    "sleep 100d"
